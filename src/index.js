import React from 'react';
import { render } from 'react-dom';
import './styles/index.css';
import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducer from './reducers';
import thunk from 'redux-thunk'
import { BrowserRouter } from 'react-router-dom'
import ReduxToastr from 'react-redux-toastr'

const store = createStore(
  reducer,
  applyMiddleware(thunk)
)

render(
  <Provider store={store}>
    <div>
      <BrowserRouter>
        <App />
      </BrowserRouter>
      <ReduxToastr
        timeOut={3000}
        newestOnTop={false}
        preventDuplicates
        position="bottom-center"
        transitionIn="fadeIn"
        transitionOut="fadeOut"
        progressBar/>
    </div>
  </Provider>,
  document.getElementById('root')
)

registerServiceWorker();
