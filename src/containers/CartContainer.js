import React, { Component } from 'react'
import '../styles/styles.css'
import CartList from '../components/CartList'
import { connect } from 'react-redux'
import { removeFromCart } from '../actions';
import { bindActionCreators } from 'redux'

class CartContainer extends Component {

    handleRemoveFromCartClick = (product) => {
        this.props.removeFromCart(product);
    }

    render() {
        return (
            <div className="cart-container">
                <div className="panel-body row">
                    {this.props.cart.length !== 0 ? 
                        <CartList
                            cart={this.props.cart}
                            onRemoveFromCartClicked={this.handleRemoveFromCartClick}/> 
                        : <p className="empty-cart-text">Empty Cart</p>}
                    
                </div>
                <div className="panel-footer checkout-panel">
                    <p className="cart-total-label">Total: <span className="cart-total">${(this.props.total).toFixed(2)}</span></p>
                    <button className="btn btn-black" disabled={this.props.cart.length === 0}>Checkout</button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
      cart: state.cart.cart,
      total: state.cart.total,
    }
  }

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        removeFromCart: removeFromCart,
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(CartContainer);
