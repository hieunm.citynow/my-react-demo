import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Home'
import Brands from './Brands'
import Detail from './Detail'

const Main = () => (
  <main>
    <Switch>
      <Route exact path='/' component={Home}/>
      <Route path='/brands' component={Brands}/>
      <Route path='/detail/:productId' component={Detail}/>
    </Switch>
  </main>
)

export default Main