import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { fetchProductsWithRedux, addToCart } from '../actions'
import { bindActionCreators } from 'redux'
import ProductDetail from '../components/ProductDetail'
import {toastr} from 'react-redux-toastr'

class Detail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      product: null,
      count: 1
    }
  }

  componentDidMount() {
    const { match: { params } } = this.props;
    this.props.fetchProductsWithRedux();
    let product = this.props.products.find(product => product.id == params.productId)
    this.setState({
      product: product
    })
  }

  handleAddToCartClick = (product, count) => {
    this.props.addToCart(product, count);
    toastr.light('Success', 'Your item has been added to your shopping cart.', { icon: 'success', status: 'success' });
  }

  onMinusClick = () => {
    if(this.state.count === 1)
        return;
    this.setState({
        count: this.state.count - 1
    })
  }


  onPlusClick = () => {
      if(this.state.count === 10) //TODO: inventory
          return;
      this.setState({
          count: this.state.count + 1
      })
  }

  render() {
    return (
      <div className="container-fluid">
        <Link to='/'>
          <h3>Back to Home</h3>
        </Link>
        {this.state.product !== null ? <ProductDetail {...this.state.product}
          onMinusClick={this.onMinusClick} onPlusClick={this.onPlusClick} count={this.state.count}
          onAddToCartClicked={() => { this.handleAddToCartClick(this.state.product, this.state.count); 
          }}/> : null }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      products: state.product.products,
      isFetching: state.product.isFetching
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
      fetchProductsWithRedux: fetchProductsWithRedux,
      addToCart: addToCart,
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail);