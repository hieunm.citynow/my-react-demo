import React, { Component } from 'react'
import '../styles/styles.css';
import ProductList from '../components/ProductList'
import { connect } from 'react-redux'
import { addToCart, fetchProductsWithRedux } from '../actions';
import { bindActionCreators } from 'redux'

class ProductsContainer extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
      this.props.fetchProductsWithRedux();
    }

    render() {
        return (
            <div className="Product-container">
                <h3>Shoes</h3>
                { this.props.isFetching ? <img className="loading" src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif" /> : 
                <div className="row">
                    <ProductList
                        products={this.props.products}   
                        />
                </div> }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.product.products,
        isFetching: state.product.isFetching
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fetchProductsWithRedux: fetchProductsWithRedux
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductsContainer);