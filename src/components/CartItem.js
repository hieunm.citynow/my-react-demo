import React from 'react';
import '../styles/styles.css';
import PropTypes from 'prop-types';

class CartItem extends React.Component { 
    constructor(props) {
        super(props);
    }
    
    render() {
        return (
            <div className="row cart-item-holder">
                <div className="col-lg-3 col-xs-6 col-sm-6 cart-item-image-holder">
                    <a href="#">
                        <img className="cart-item-image" src={this.props.image} alt={this.props.name} />
                    </a>
                </div>
                <div className="col-lg-3 col-xs-6 col-sm-6 cart-name-holder">
                    <p className="cart-item-name">{this.props.name}</p>
                    <p className="cart-item-price">${(this.props.price).toFixed(2)} x {this.props.count}</p>
                </div>
                <div className="col-lg-3 col-xs-6 col-sm-6 flex-container">
                    <button 
                        className="btn btn-black"
                        onClick={this.props.onMinusClick}
                        ><i className="fa fa-minus icon-size"></i></button>
                    <p className="num"> {this.props.count} </p>
                    <button 
                        className="btn btn-black"
                        onClick={this.props.onPlusClick}
                        ><i className="fa fa-plus icon-size"></i></button>
                </div>
                <div className="col-lg-3 col-xs-6 col-sm-6 flex-container">
                    <button
                        className="btn btn-black"
                        onClick={this.props.onRemoveFromCartClicked}>Remove</button>
                </div>
            </div>
        )
    }
}

CartItem.propTypes = {
    onRemoveFromCartClicked: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired, // unit price of the product
    count: PropTypes.number.isRequired,
    //brand: PropTypes.string,
    image: PropTypes.string,
}

CartItem.defaultProps = {
    image: "http://www.pixedelic.com/themes/geode/demo/wp-content/uploads/sites/4/2014/04/placeholder4.png",
    count: 1,
}

export default CartItem;