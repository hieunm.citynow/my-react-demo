import React from 'react'
import ProductItem from '../components/ProductItem'
import PropTypes from 'prop-types'

class ProductList extends React.Component{
    render() {
        return (
            this.props.products.map((product) => (
                <div key={product.id} className="col-lg-4 col-md-4 col-sm-4">
                    <ProductItem {...product}
                        //onAddToCartClicked={() => { this.props.onAddToCartClicked(product); }}
                        /> 
                </div> 
            ))
        )
    }
}

ProductList.propTypes = {
    products: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        inventory: PropTypes.number.isRequired,
        price: PropTypes.number.isRequired,
        image: PropTypes.string,
        brand: PropTypes.string,
        //count: PropTypes.number.isRequired,
      }).isRequired
    ).isRequired,
    //onAddToCartClicked: PropTypes.func.isRequired
}

ProductList.defaultProps = {
    //products: [{ "id": 0, "name": "WARD HI HIGH-TOP SNEAKER", "image": "https://dsw.scene7.com/is/image/DSWShoes/393645_001_ss_01?$pdp-image$", "brand": "Van", "price": 59.99, "inventory": 10 }]
}

export default ProductList