import React from 'react';
import '../styles/styles.css';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'

class ProductItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            count: 1
        }
    }

    onMinusClick = () => {
        if(this.state.count === 1)
            return;
        this.setState({
            count: this.state.count - 1
        })
    }


    onPlusClick = () => {
        if(this.state.count === this.props.inventory)
            return;
        this.setState({
            count: this.state.count + 1
        })
    }
    
    render() {
        return (
            <div className="product-item-holder grow">
                <Link className="product-link" to={`/detail/${this.props.id}`}>
                    <img className="product-item-image" src={this.props.image} alt={this.props.name} />
                </Link>
                <p className="product-item-name">{this.props.brand}</p>
                <Link className="product-link" to={`/detail/${this.props.id}`}>
                    <p className="product-item-name">{this.props.name}</p>
                </Link>
                <p className="product-item-price">${(this.props.price * this.state.count).toFixed(2)}</p>
                {/* <div className="flex-container">
                    <button 
                        className="btn btn-black"
                        onClick={this.onMinusClick}
                        ><i className="fa fa-minus icon-size"></i></button>
                    <p className="num"> {this.state.count} </p>
                    <button 
                        className="btn btn-black"
                        onClick={this.onPlusClick}
                        ><i className="fa fa-plus icon-size"></i></button>
                </div>
                <button
                    className="btn btn-black"
                    onClick={this.props.onAddToCartClicked}
                    disabled={this.props.inventory > 0 ? '' : 'disabled'}>
                    {this.props.inventory > 0 ? 'Add to cart' : 'Sold Out'}
                </button> */}
            </div>
        )
    }
}

ProductItem.propTypes = {
    //onAddToCartClicked: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    inventory: PropTypes.number.isRequired,
    price: PropTypes.number.isRequired,
    //count: PropTypes.number.isRequired,
    brand: PropTypes.string,
    image: PropTypes.string,
}

ProductItem.defaultProps = {
    image: "http://www.pixedelic.com/themes/geode/demo/wp-content/uploads/sites/4/2014/04/placeholder4.png",
    count: 1,
}

export default ProductItem;