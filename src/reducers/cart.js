import  * as types from '../constants/ActionTypes';

const initialState = {
  cart: [], 
  total: 0
}

const handleAddToCart = (state, action) => {
  
}

export default (state = initialState, action) => {
  switch (action.type) {
    case types.ADD_TO_CART:
      let oldState = state;
      let flag = 0;
      oldState.cart.forEach(item => {
        if(action.payload.product == item.product) {
          item.count += action.payload.count;
          flag++;
        }
      })
      if(flag > 0) {
        let newTotal = 0;
        oldState.cart.forEach(item=>{
            newTotal += (item.product.price * item.count);
        });
        oldState.total = newTotal;
        return oldState;
      }
      else {
        let newstate = {
            ...state,
            cart: [...state.cart, action.payload]
          };
        let newTotal = 0;
        newstate.cart.forEach(item=>{
            newTotal += (item.product.price * item.count);
        });
        newstate.total = newTotal;
        return newstate; 
      }
    case types.REMOVE_FROM_CART:
      let removeState =  {
          ...state, 
          cart: state.cart.filter((item) => action.payload !== item)
        };
      let total = 0;
      removeState.cart.forEach(item=>{
        total += (item.product.price * item.count);
      });
      removeState.total = total;
      return removeState; 
    default:
      return state;
  }
};