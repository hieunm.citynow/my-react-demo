// import  * as types from '../constants/ActionTypes';
// import products from '../api/products.json'

// export default (state = { products }, action) => {
//     switch (action.type) {
//       case types.GET_PRODUCTS:
//         return { products: [...state.products]};
//       default:
//         return state;
//     }
// };

import  * as types from '../constants/ActionTypes';
import data from '../data/products'

// const initialState = { products: data, isFetching: false }
const initialState = { products: [], isFetching: true }

export default (state = initialState, action) => {
  switch (action.type) {
    case types.GET_PRODUCTS:
      return {
        products: [...state.products],
        isFetching: false
      };
    case types.FETCH_PRODUCTS_REQUEST:
      return { 
        ...state,
        isFetching: true
      };
    case types.FETCH_PRODUCTS_SUCCESS:
      return {
        products: action.payload,
        isFetching: false
      };
    case types.FETCH_PRODUCTS_FAILURE:
      return {
        ...state,
        isFetching: false
      }
    default:
      return state;
  }
}