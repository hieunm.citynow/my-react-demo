import  * as types from '../constants/ActionTypes'
  
const addToCart = (product, count) => {
    return {
        type: types.ADD_TO_CART,
        payload: {
            product: product,
            count: count
        }
    }
};

const removeFromCart = (product) => {
    return {
        type: types.REMOVE_FROM_CART,
        payload: product
    }
};

const getProducts = () => {
    return {
        type: types.GET_PRODUCTS,
        payload: ''
    } 
}

const fetchProductsRequest = () => {
    return {
        type: types.FETCH_PRODUCTS_REQUEST,
    }
}

/* fetch products successfully */
const fetchProductsSuccess = (products) => {
    return {
        type: types.FETCH_PRODUCTS_SUCCESS,
        payload: products
    }
}

const fetchProductsFailure = (message) => {
    return {
        type: types.FETCH_PRODUCTS_FAILURE,
        payload: message
    }
}


const fetchProducts = () => {
    const URL = "http://5a6ae3898bdfbe0012adc169.mockapi.io/api/v1/products";
    return fetch(URL, { method: 'GET'}).then(response => Promise.all([response, response.json()]));
  }

const fetchProductsWithRedux = () => {
    return function (dispatch) {
      dispatch(fetchProductsRequest())
      return fetchProducts()
        .then(([response, json]) => {
          if (response.status >= 400) {
            dispatch(fetchProductsFailure("Bad response from server"));
          }
          return json;
        })
        .then(json => {
            setTimeout(() => {
                dispatch(fetchProductsSuccess(json))
            }, 500)
        });
    }
}
export { addToCart, removeFromCart, fetchProductsWithRedux }